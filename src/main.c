#include "main.h"

#if INTERFACE
#include "board.h"
#include "engine/engine.h"
#include "engine/gamestate.h"
#include "stdbool.h"
#include "teststate.h"
#include "tile.h"
#include <allegro5/allegro.h>
#include <stdio.h>
#endif

// later: #C48200

static char *_signature = "Project Coal - made by nexustix";

static TestState *teststate;

// char *cake = "cheeese";

bool main_on_event(ALLEGRO_EVENT event) {
  return teststate_on_event(teststate, event);
}
void main_on_update(double dt) { teststate_on_update(teststate, dt); }
void main_on_draw() { teststate_on_draw(teststate); }

int WinMain() {
  main();
  return 0;
}

int main() {
  strcat(_signature, "");
  srand(time(0));
  unsigned char tile_count = 16;
  unsigned char tile_width = 16 + 8;
  unsigned char space = 8;
  teststate = newTestState(tile_count);

  unsigned char x;
  unsigned char y;
  unsigned char random;
  unsigned char cutoff = 32;
  for (x = 0; x < teststate->minesweeper->board->width; x = x + 1) {
    for (y = 0; y < teststate->minesweeper->board->height; y = y + 1) {
      random = rand() % 0xFF;
      if (random < cutoff) {
        board_set_kind_at(teststate->minesweeper->board, TILE_MINE, x, y);
        // board_set_kind_at(teststate->board, TILE_EMPTY, x, y);
      } else {
        board_set_kind_at(teststate->minesweeper->board, TILE_EMPTY, x, y);
      }
    }
  }
  // board_set_kind_at(teststate->board, TILE_MINE, 3, 3);
  // board_set_kind_at(teststate->board, TILE_MINE, 3, 4);
  // board_set_kind_at(teststate->board, TILE_MINE, 4, 3);
  // board_set_kind_at(teststate->board, TILE_MINE, 5, 3);
  // board_set_kind_at(teststate->board, TILE_MINE, 3, 5);
  // board_set_kind_at(teststate->board, TILE_MINE, 5, 5);
  // board_set_kind_at(teststate->board, TILE_MINE, 5, 4);
  // board_set_kind_at(teststate->board, TILE_MINE, 4, 5);

  int screen_w = space + (tile_width * tile_count) + (space * tile_count);
  int screen_h = screen_w;
  printf("%d:%d\n", screen_w, screen_h);
  Engine *engine = newEngine(screen_w, screen_h);

  engine->on_event = main_on_event;
  engine->on_update = main_on_update;
  engine->on_draw = main_on_draw;
  engine_run(engine);

  // e.cheese_update = &cake_update;
  // e.cheese_update = cake_update;
  // e.on_update = cake_update;
  // engine_run(&e);
  return 0;
}
