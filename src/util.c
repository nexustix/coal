#include "util.h"

#include <stdlib.h>

#if INTERFACE
typedef struct {
  int x;
  int y;
} Coord2d;
#endif

Coord2d *newCoord2D(int x, int y) {
  Coord2d *coord2d = malloc(sizeof(*coord2d));
  coord2d->x = x;
  coord2d->y = y;
  return coord2d;
}

int oned_as_twod_x(int offset, int width) { return (offset % width); }
int oned_as_twod_y(int offset, int width) { return (offset / width); }

void oned_to_twod(int offset, int width, Coord2d *result) {
  result->x = offset % width;
  result->y = offset / width;
}

int twod_to_oned(int x, int y, int width) { return (x + (y * width)); }
