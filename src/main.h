/* This file was automatically generated.  Do not edit! */
#ifndef SRC_MAIN_H
#define SRC_MAIN_H
#include "board.h"
#include "engine/engine.h"
#include "engine/gamestate.h"
#include "stdbool.h"
#include "teststate.h"
#include "tile.h"
#include <allegro5/allegro.h>
#include <stdio.h>
int main();
int WinMain();
void main_on_draw();
void main_on_update(double dt);
bool main_on_event(ALLEGRO_EVENT event);
#define INTERFACE 0
#endif
