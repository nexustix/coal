#include "numbers.h"

#if INTERFACE
#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
#endif

int numbers_draw_one(int x, int y, int r, int tile_width,
                     ALLEGRO_COLOR colour) {
  al_draw_circle(x + (tile_width / 2), y + (tile_width / 2), r, colour, 2);
  return 1;
}
int numbers_draw_two(int x, int y, int off, int tile_width,
                     ALLEGRO_COLOR colour) {
  al_draw_line(x + (tile_width / 2), y + off, x + (tile_width / 2),
               y + tile_width - off, colour, 2);
  return 1;
}
int numbers_draw_four(int x, int y, int off, int tile_width,
                      ALLEGRO_COLOR colour) {
  al_draw_line(x + off, y + (tile_width / 2), x + tile_width - off,
               y + (tile_width / 2), colour, 2);
  return 1;
}

int numbers_draw_eight(int x, int y, int r, int tile_width,
                       ALLEGRO_COLOR colour) {
  al_draw_filled_circle(x + (tile_width / 2), y + (tile_width / 2), r, colour);
  return 1;
}
