#include "minesweeper.h"

#if INTERFACE
#include "../board.h"
#include <stdbool.h>
typedef struct {
  Board *board;
  bool game_over;
  bool game_won;
  bool moved;
} Minesweeper;
// typedef enum {
//} Unvover_Result;
#endif

Minesweeper *newMinesweeper(unsigned char board_size) {
  Minesweeper *minesweeper = calloc(1, sizeof(*minesweeper));
  minesweeper->board = newBoard(board_size);
  return minesweeper;
}

bool minesweeper_uncover(Minesweeper *minesweeper, unsigned char x,
                         unsigned char y) {
  bool exploded = board_uncover(minesweeper->board, x, y);
  if (exploded) {
    if (!minesweeper->moved) {
      board_set_kind_at(minesweeper->board, TILE_EMPTY, x, y);
      minesweeper->moved = true;
      return false;
    } else {
      minesweeper->game_over = true;
    }
  }
  return exploded;
}