/* This file was automatically generated.  Do not edit! */
#ifndef SRC_GAME_MINESWEEPER_H
#define SRC_GAME_MINESWEEPER_H
#include "../board.h"
#include <stdbool.h>
typedef struct {
  Board *board;
  bool game_over;
  bool game_won;
  bool moved;
}Minesweeper;
bool minesweeper_uncover(Minesweeper *minesweeper,unsigned char x,unsigned char y);
Minesweeper *newMinesweeper(unsigned char board_size);
#define INTERFACE 0
#endif
