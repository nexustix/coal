/* This file was automatically generated.  Do not edit! */
#ifndef SRC_TESTSTATE_H
#define SRC_TESTSTATE_H
#include "board.h"
#include "game/minesweeper.h"
#include "numbers.h"
#include "tile.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct {
  unsigned char board_size;
  // Board *board;
  Minesweeper *minesweeper;
  ALLEGRO_COLOR tile_idle_colour;
  // ALLEGRO_COLOR tile_hover_colour;
  // ALLEGRO_COLOR hint_colour;
  unsigned char selection_x;
  unsigned char selection_y;
  // bool hovering;
  // bool clicking;
  bool game_over;
  bool game_won;
  bool moved;
}TestState;
void teststate_draw_board(TestState *ts,Minesweeper *teststate);
void teststate_on_draw(TestState *teststate);
void teststate_on_update(TestState *teststate,double dt);
bool teststate_on_event(TestState *teststate,ALLEGRO_EVENT event);
void teststate_init(TestState *teststate);
TestState *newTestState(unsigned char board_size);
#define INTERFACE 0
#endif
