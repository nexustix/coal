/* This file was automatically generated.  Do not edit! */
#ifndef SRC_ENGINE_ENGINE_H
#define SRC_ENGINE_ENGINE_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <stdbool.h>
#include <stdio.h>
typedef struct {
  int screen_width;
  int screen_height;
  bool (*on_event)(ALLEGRO_EVENT event);
  void (*on_update)(double dt);
  void (*on_draw)();
}Engine;
int engine_run(Engine *engine);
Engine *newEngine(int screen_width,int screen_height);
void __engine_on_draw();
void __engine_on_update(double dt);
bool __engine_on_event(ALLEGRO_EVENT event);
#define INTERFACE 0
#endif
