#include "engine.h"

//#include <stdbool.h>
//#include "../thing.h"

#if INTERFACE

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <stdbool.h>
#include <stdio.h>
typedef struct {
  int screen_width;
  int screen_height;
  bool (*on_event)(ALLEGRO_EVENT event);
  void (*on_update)(double dt);
  void (*on_draw)();
} Engine;

// bool engine_on_event(ALLEGRO_EVENT event);
// void engine_on_update(double dt);
// void engine_on_draw();
#endif

bool __engine_on_event(ALLEGRO_EVENT event) { return false; }
void __engine_on_update(double dt) {}
void __engine_on_draw() { al_clear_to_color(al_map_rgb(16, 128, 16)); }

Engine *newEngine(int screen_width, int screen_height) {
  Engine *engine = malloc(sizeof(*engine));
  engine->screen_width = screen_width;
  engine->screen_height = screen_height;
  engine->on_event = __engine_on_event;
  engine->on_update = __engine_on_update;
  engine->on_draw = __engine_on_draw;
  return engine;
}

int engine_run(Engine *engine) {
  ALLEGRO_DISPLAY *display;
  ALLEGRO_EVENT_QUEUE *queue;
  ALLEGRO_TIMER *timer;

  al_init();
  al_init_primitives_addon();
  display = al_create_display(engine->screen_width, engine->screen_height);
  queue = al_create_event_queue();
  timer = al_create_timer(1.0 / 60.0);

  al_install_keyboard();
  al_install_mouse();
  al_register_event_source(queue, al_get_keyboard_event_source());
  al_register_event_source(queue, al_get_display_event_source(display));
  al_register_event_source(queue, al_get_mouse_event_source());
  al_register_event_source(queue, al_get_timer_event_source(timer));

  al_start_timer(timer);

  // double cur_time = al_get_time();
  // double last_time = al_get_time();
  // double delta = 0;
  double dt = 0;
  // Thing *cake = newThing();
  // printf(cake->texture);

  ALLEGRO_EVENT event;
  bool running = true;
  while (running) {

    al_wait_for_event(queue, &event);
    if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
      running = false;
    }

    if (event.type == ALLEGRO_EVENT_TIMER) {
      dt = 1.0;

      // engine_on_update(dt);
      // engine.cheese
      engine->on_update(dt);

      // cake->position.x += 1 * dt;
      // cake->position.y += 1 * dt;

      al_clear_to_color(al_map_rgb(16, 16, 16));
      // al_draw_circle(cake->position.x, cake->position.y, 10,
      //               al_map_rgb(200, 200, 200), 1);
      // al_draw_circle(20, 20, 10, al_map_rgb(200, 200, 200), 1);
      // engine_on_draw();
      engine->on_draw();
      al_flip_display();
    }
    // engine_on_event(event);
    engine->on_event(event);
  }

  al_stop_timer(timer);
  // free(cake);
  // al_uninstall_mouse();
  al_destroy_timer(timer);
  al_uninstall_keyboard();
  al_destroy_event_queue(queue);
  al_destroy_display(display);
  al_shutdown_primitives_addon();
  al_uninstall_system();

  // exit(0);

  return 0;
}
