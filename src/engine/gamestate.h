/* This file was automatically generated.  Do not edit! */
#ifndef SRC_ENGINE_GAMESTATE_H
#define SRC_ENGINE_GAMESTATE_H
#include <allegro5/allegro.h>
#include <stdbool.h>
#include <stdio.h>
typedef struct {
  bool (*on_event)(ALLEGRO_EVENT event);
  void (*on_update)(double dt);
  void (*on_draw)();
}Gamestate;
Gamestate *newGamestate();
void __gamestate_on_draw();
void __gamestate_on_update(double dt);
bool __gamestate_on_event(ALLEGRO_EVENT event);
#define INTERFACE 0
#endif
