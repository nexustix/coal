#include "gamestate.h"

#if INTERFACE
#include <allegro5/allegro.h>
#include <stdbool.h>
#include <stdio.h>
typedef struct {
  bool (*on_event)(ALLEGRO_EVENT event);
  void (*on_update)(double dt);
  void (*on_draw)();
} Gamestate;
#endif

bool __gamestate_on_event(ALLEGRO_EVENT event) { return false; }
void __gamestate_on_update(double dt) {}
void __gamestate_on_draw() { al_clear_to_color(al_map_rgb(16, 16, 16)); }

Gamestate *newGamestate() {
  Gamestate *gamestate = malloc(sizeof(*gamestate));
  gamestate->on_event = __gamestate_on_event;
  gamestate->on_update = __gamestate_on_update;
  gamestate->on_draw = __gamestate_on_draw;
  return gamestate;
}
