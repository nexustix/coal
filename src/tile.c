#include "tile.h"

#if INTERFACE
#include <stdbool.h>
typedef enum { TILE_VOID, TILE_EMPTY, TILE_MINE } Tilekind;
typedef struct {
  Tilekind kind;
  bool uncovered;
  bool flagged;
} Tile;
#endif

Tile makeTile(Tilekind tilekind) {
  Tile tile;
  tile.kind = tilekind;
  tile.flagged = false;
  tile.uncovered = false;
  return tile;
}

bool tile_is_solved(Tile *tile) {
  if (tile->kind == TILE_MINE) {
    return tile->flagged;
  } else {
    return tile->uncovered;
  }
  return false;
}