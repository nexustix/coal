/* This file was automatically generated.  Do not edit! */
#ifndef SRC_BOARD_H
#define SRC_BOARD_H
#include "tile.h"
#include "util.h"
#include <stdlib.h>
typedef struct {
  unsigned int linear_size;
  unsigned char width;
  unsigned char height;
  Tile *tiles;
}Board;
bool board_toggle_flag(Board *board,unsigned char x,unsigned char y);
bool board_uncover(Board *board,unsigned char x,unsigned char y);
unsigned char board_get_danger(Board *board,unsigned char x,unsigned char y);
bool board_is_dangerous_at(Board *board,unsigned char x,unsigned char y);
Tilekind board_get_kind_at(Board *board,unsigned char x,unsigned char y);
int board_set_kind_at(Board *board,Tilekind value,unsigned char x,unsigned char y);
int board_get_tile_at(Board *board,Tile **tile,unsigned char x,unsigned char y);
Board *newBoard(unsigned char size);
#define INTERFACE 0
#endif
