/* This file was automatically generated.  Do not edit! */
#ifndef SRC_UTIL_H
#define SRC_UTIL_H
int twod_to_oned(int x,int y,int width);
typedef struct {
  int x;
  int y;
}Coord2d;
void oned_to_twod(int offset,int width,Coord2d *result);
int oned_as_twod_y(int offset,int width);
int oned_as_twod_x(int offset,int width);
Coord2d *newCoord2D(int x,int y);
#define INTERFACE 0
#endif
