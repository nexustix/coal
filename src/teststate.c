#include "teststate.h"

#if INTERFACE
#include "board.h"
#include "game/minesweeper.h"
#include "numbers.h"
#include "tile.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct {
  unsigned char board_size;
  // Board *board;
  Minesweeper *minesweeper;
  ALLEGRO_COLOR tile_idle_colour;
  // ALLEGRO_COLOR tile_hover_colour;
  // ALLEGRO_COLOR hint_colour;
  unsigned char selection_x;
  unsigned char selection_y;
  // bool hovering;
  // bool clicking;
  bool game_over;
  bool game_won;
  bool moved;
} TestState;
#endif

TestState *newTestState(unsigned char board_size) {
  // TestState *teststate = malloc(sizeof(*teststate));
  TestState *teststate = calloc(1, sizeof(*teststate));
  teststate->tile_idle_colour = al_map_rgb(128 + 64, 128, 0);
  // teststate->tile_hover_colour = al_map_rgb(128 + 16, 64 + 32, 0);
  // teststate->hint_colour = al_map_rgb(0, 128 + 64, 0);
  // teststate->board_size = board_size;
  // teststate->board = newBoard(board_size);
  teststate->minesweeper = newMinesweeper(board_size);
  return teststate;
}

void teststate_init(TestState *teststate) {}

bool teststate_on_event(TestState *teststate, ALLEGRO_EVENT event) {
  if (event.type == ALLEGRO_EVENT_MOUSE_AXES) {
    teststate->selection_x = (event.mouse.x - 8) / (16 + 8 + 8);
    teststate->selection_y = (event.mouse.y - 8) / (16 + 8 + 8);
    // printf("%d\n", event.mouse.x);
    // printf("%d\n", selected_x);
  } else if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
    if (event.mouse.button == 1) {
      if (!teststate->minesweeper->game_over) {
        minesweeper_uncover(teststate->minesweeper, teststate->selection_x,
                            teststate->selection_y);
        // bool exploded = board_uncover(teststate->board,
        // teststate->selection_x,
        //                              teststate->selection_y);
        // if (exploded) {
        //  if (teststate->moved) {
        //    teststate->game_over = true;
        //  } else {
        //    board_set_kind_at(teststate->board, TILE_EMPTY,
        //                      teststate->selection_x, teststate->selection_y);
        //    board_uncover(teststate->board, teststate->selection_x,
        //                  teststate->selection_y);
        //  }
        //} else {
        //  teststate->moved = true;
        //}
      }
    } else if (event.mouse.button == 2) {
      if (!teststate->game_over) {
        board_toggle_flag(teststate->minesweeper->board, teststate->selection_x,
                          teststate->selection_y);
      }
    }
  }

  return false;
}

void teststate_on_update(TestState *teststate, double dt) {}

void teststate_on_draw(TestState *teststate) {
  teststate_draw_board(teststate, teststate->minesweeper);
}

void teststate_draw_board(TestState *ts, Minesweeper *teststate) {
  static Tile *tile;

  unsigned char x;
  unsigned char y;

  unsigned int tx;
  unsigned int ty;

  unsigned int tilew = 16 + 8;
  unsigned int tileh = 16 + 8;
  unsigned int space = 8;

  unsigned char danger;
  unsigned char first_bit;
  unsigned char second_bit;
  unsigned char third_bit;
  unsigned char fourth_bit;

  // state->tile_idle_colour = al_map_rgb(128 + 64, 128, 0);

  // Tilekind tile_kind;

  // ALLEGRO_COLOR colour = al_map_rgb(128 + 64, 128, 0);
  // ALLEGRO_COLOR colour = state->tile_idle_colour;
  ALLEGRO_COLOR tile_idle_colour = al_map_rgb(128 + 64, 128, 0);
  ALLEGRO_COLOR tile_hover_colour = al_map_rgb(128 + 16, 64 + 32, 0);
  // ALLEGRO_COLOR tile_dark_colour = al_map_rgb(128 + 8, 32 + 32, 0);
  // ALLEGRO_COLOR void_colour = al_map_rgb(128 + 16, 64 + 32, 0);
  ALLEGRO_COLOR empty_colour = al_map_rgb(32, 32, 32);
  ALLEGRO_COLOR missing_colour = al_map_rgb(64, 32, 64);
  // ALLEGRO_COLOR hint_colour = al_map_rgb(96, 64, 0);
  ALLEGRO_COLOR hint_colour = al_map_rgb(128 + 16, 64 + 32, 0);

  al_clear_to_color(al_map_rgb(16 + 8, 16 + 8, 16 + 8));

  // al_draw_rectangle(10, 10, 20, 20, al_map_rgb(255, 0, 0), 1);
  for (x = 0; x < teststate->board->width; x = x + 1) {
    for (y = 0; y < teststate->board->height; y = y + 1) {
      tx = space + (tilew * x) + (space * x);
      ty = space + (tileh * y) + (space * y);

      board_get_tile_at(teststate->board, &tile, x, y);
      // tile = *ptile;
      // printf("%d", tile_kind);
      if (tile->uncovered) {
        al_draw_filled_rectangle(tx, ty, tx + tilew, ty + tileh, empty_colour);
        danger = board_get_danger(teststate->board, x, y);
        // first_bit = danger % 0x2;
        // second_bit = (danger / 0x2) % 0x2;
        // third_bit = (danger / 0x4) % 0x2;
        // fourth_bit = (danger / 0x6) % 0x2;
        first_bit = (danger >> 0) & 1;
        second_bit = (danger >> 1) & 1;
        third_bit = (danger >> 2) & 1;
        fourth_bit = (danger >> 3) & 1;
        ;

        if (first_bit &&
            numbers_draw_one(tx, ty, tilew / 4, tilew, hint_colour))
          ;
        if (second_bit && numbers_draw_two(tx, ty, 2, tilew, hint_colour))
          ;

        if (third_bit && numbers_draw_four(tx, ty, 2, tilew, hint_colour))
          ;

        if (!first_bit && !second_bit && !third_bit && fourth_bit &&
            numbers_draw_eight(tx, ty, tilew / 3, tilew, hint_colour))
          ;

      } else {
        if (tile->kind == TILE_VOID) {
          al_draw_filled_rectangle(tx, ty, tx + tilew, ty + tileh,
                                   missing_colour);
        } else {
          al_draw_filled_rectangle(tx, ty, tx + tilew, ty + tileh,
                                   tile_idle_colour);

          if (teststate->game_over && tile->kind == TILE_MINE) {
            al_draw_filled_rectangle(tx + 4, ty + 4, tx + tilew - 4,
                                     ty + tileh - 4, empty_colour);
          }
          if (tile->flagged) {
            if (teststate->game_over) {
              al_draw_filled_rectangle(tx + 8, ty + 8, tx + tilew - 8,
                                       ty + tileh - 8, tile_hover_colour);
            } else {
              al_draw_rectangle(tx + 8, ty + 8, tx + tilew - 8, ty + tileh - 8,
                                empty_colour, 4);
            }
          }
        }
        // puts("foobar");
      }

      if (x == ts->selection_x && y == ts->selection_y) {
        al_draw_rectangle(tx - 4, ty - 4, tx + tilew + 4, ty + tileh + 4,
                          tile_idle_colour, 2);
      }
    }
  }
}