#include "board.h"

#if INTERFACE
#include "tile.h"
#include "util.h"
#include <stdlib.h>
typedef struct {
  unsigned int linear_size;
  unsigned char width;
  unsigned char height;
  Tile *tiles;
} Board;
#endif

Board *newBoard(unsigned char size) {
  Board *board = malloc(sizeof(*board));
  board->linear_size = size * size;
  board->width = size;
  board->height = size;
  board->tiles = calloc(board->linear_size, sizeof(Tile));
  unsigned int offset;
  for (offset = 0; offset < board->linear_size; offset = offset + 1) {
    board->tiles[offset] = makeTile(TILE_VOID);
  }
  return board;
}

int board_get_tile_at(Board *board, Tile **tile, unsigned char x,
                      unsigned char y) {
  if (x >= 0 && y >= 0 && x < board->width && y < board->height) {
    int offset = twod_to_oned(x, y, board->width);
    *tile = (Tile *)&board->tiles[offset];
    return 0;
  }
  return 1;
}

int board_set_kind_at(Board *board, Tilekind value, unsigned char x,
                      unsigned char y) {
  if (x >= 0 && y >= 0 && x < board->width && y < board->height) {
    int offset = twod_to_oned(x, y, board->width);
    board->tiles[offset].kind = value;
    return 0;
  }
  return 1;
}

Tilekind board_get_kind_at(Board *board, unsigned char x, unsigned char y) {
  if (x >= 0 && y >= 0 && x < board->width && y < board->height) {
    int offset = twod_to_oned(x, y, board->width);
    return board->tiles[offset].kind;
  }
  return TILE_VOID;
}

bool board_is_dangerous_at(Board *board, unsigned char x, unsigned char y) {
  return (board_get_kind_at(board, x, y) == TILE_MINE);
}

unsigned char board_get_danger(Board *board, unsigned char x, unsigned char y) {
  return board_is_dangerous_at(board, x - 1, y - 1) +
         board_is_dangerous_at(board, x, y - 1) +
         board_is_dangerous_at(board, x - 1, y) +
         board_is_dangerous_at(board, x + 1, y + 1) +
         board_is_dangerous_at(board, x, y + 1) +
         board_is_dangerous_at(board, x + 1, y) +
         board_is_dangerous_at(board, x + 1, y - 1) +
         board_is_dangerous_at(board, x - 1, y + 1);
}

bool board_uncover(Board *board, unsigned char x, unsigned char y) {
  if (x >= 0 && y >= 0 && x < board->width && y < board->height) {
    int offset = twod_to_oned(x, y, board->width);
    if (!board->tiles[offset].flagged) {
      if (!board->tiles[offset].uncovered) {
        if (board_is_dangerous_at(board, x, y)) {
          return true;
        }
        board->tiles[offset].uncovered = true;
        if (board_get_danger(board, x, y) == 0) {
          board_uncover(board, x - 1, y - 1);
          board_uncover(board, x, y - 1);
          board_uncover(board, x - 1, y);
          board_uncover(board, x + 1, y + 1);
          board_uncover(board, x, y + 1);
          board_uncover(board, x + 1, y);
          board_uncover(board, x + 1, y - 1);
          board_uncover(board, x - 1, y + 1);
        }
      }
    }
    // board->tiles[offset].uncovered = true;
  }

  return false;
}

bool board_toggle_flag(Board *board, unsigned char x, unsigned char y) {
  if (x >= 0 && y >= 0 && x < board->width && y < board->height) {
    int offset = twod_to_oned(x, y, board->width);
    board->tiles[offset].flagged = !board->tiles[offset].flagged;
    return true;
  }
  return false;
}
