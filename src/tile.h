/* This file was automatically generated.  Do not edit! */
#ifndef SRC_TILE_H
#define SRC_TILE_H
#include <stdbool.h>
typedef enum { TILE_VOID, TILE_EMPTY, TILE_MINE }Tilekind;
typedef struct {
  Tilekind kind;
  bool uncovered;
  bool flagged;
}Tile;
bool tile_is_solved(Tile *tile);
Tile makeTile(Tilekind tilekind);
#define INTERFACE 0
#endif
