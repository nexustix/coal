/* This file was automatically generated.  Do not edit! */
#ifndef SRC_NUMBERS_H
#define SRC_NUMBERS_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
int numbers_draw_eight(int x,int y,int r,int tile_width,ALLEGRO_COLOR colour);
int numbers_draw_four(int x,int y,int off,int tile_width,ALLEGRO_COLOR colour);
int numbers_draw_two(int x,int y,int off,int tile_width,ALLEGRO_COLOR colour);
int numbers_draw_one(int x,int y,int r,int tile_width,ALLEGRO_COLOR colour);
#define INTERFACE 0
#endif
